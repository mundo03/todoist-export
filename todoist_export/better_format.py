"""Todoist Sync data is flat, organizae it by project, section, task, description."""

from .todoist import Todoist
import json
import typer


class BetterFormat():
    """Maps Todoist data in a way that makes sense."""

    def __init__(self):
        """Get Todoist data."""
        td = Todoist()
        self.data = td.full_sync()

    def _get_projects(self):
        """Get Todoist's Projects."""
        self.projects = self.data.get('projects', [])

    def _get_project_notes(self):
        """Get Todoist's notes."""
        self.project_notes = self.data.get('project_notes', [])

    def _get_sections(self):
        """Get Todoist's Sections."""
        self.sections = self.data.get('sections', [])

    def _get_tasks(self):
        """Get Todoist's Sections."""
        self.tasks = self.data.get('items', [])

    def _get_notes(self):
        """Get Todoist's notes."""
        self.notes = self.data.get('notes', [])

    def _get_all(self):
        """Get them all."""
        self._get_projects()
        self._get_project_notes()
        self._get_sections()
        self._get_tasks()
        self._get_notes()

    def generate(self):
        """Generate herarchical data schema.

        Projects
            - Project Notes
            - Tasks (With out a section)
                - Notes
            - Sections
                - Tasks
                    - Notes
        """
        self._get_all()
        self.better_data = self.projects
        # Add Notes to Tasks
        # Notes are sorted from old to new
        tasks = self.tasks
        for task in tasks:
            task['notes'] = []
            for note in self.notes:
                if note['item_id'] == task['id']:
                    task['notes'].append(note)

        for project in self.better_data:
            # Add Project notes to Project
            project['notes'] = []
            for project_note in self.project_notes:
                if project_note['project_id'] == project['id']:
                    project['notes'].append(project_note)
            # Add Sections to Project
            project['sections'] = []

            for section in self.sections:
                if section['project_id'] == project['id']:
                    project['sections'].append(section)
                # Add Tasks to Section
                section['tasks'] = []
                for task in tasks:
                    if task['section_id'] == section['id']:
                        section['tasks'].append(task)

            # Add task to Project
            project['tasks'] = []  # Tasks with out a section
            for task in tasks:
                if task['project_id'] == project['id'] and task['section_id'] is None:
                    project['tasks'].append(task)
        return self.better_data

    def json(self):
        """Output JSON."""
        self.generate()
        return json.dumps(self.better_data, indent=4)


def better_data_json():
    """Poetry Command Access to print json to STDOUT."""
    app = typer.Typer()

    @app.command()
    def print_export():
        bf = BetterFormat()
        print(bf.json())

    app()

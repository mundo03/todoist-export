"""Generate Markdown files out of Todoist Projects's better format."""

from .better_format import BetterFormat
import snakemd
import typer


class Todoist_Markdown():
    """Todoist Projects to Markdown."""

    def __init__(self):
        """Initialize with Todoist's better format."""
        self.projects = BetterFormat().generate()
        self.md = snakemd

    def generate(self) -> dict:
        """Generate markdown for each file.

        Returns:
            dict: dictionary of snakemd Documents
        """
        projects_md = {}
        for project in self.projects:
            project_file_name = f"{project['id']}_{project['name']}"
            projects_md[project_file_name] = self.__project_to_markdown(project)
        return projects_md

    def __project_to_markdown(self, project: dict) -> snakemd.Document:
        """Structure Project as Markdown.

        # Title: Project
        Notes
        - [ ] tasks with out Section
            - Notes
        ## Section
        - [ ] Task in section
            - Notes

        Returns:
            snakemd.Document: Markdown Document
        """
        doc = self.md.Document(project['name'])
        # Add Header
        doc.add_header(project['name'])
        # Add project notes.
        for note in project['notes']:
            doc.add_paragraph(note['content'])
        # Add Project Tasks.
        tasks_mdlist = self.__tasks_to_markdown(project['tasks'])
        if tasks_mdlist:
            doc.add_element(tasks_mdlist)
        # Add Sections
        for section in project['sections']:
            doc.add_header(section['name'], level=1)
            # Add Section Tasks
            tasks_mdlist = self.__tasks_to_markdown(section['tasks'])
            if tasks_mdlist:
                doc.add_element(tasks_mdlist)
        return doc

    def __tasks_to_markdown(self, tasks: list) -> snakemd.MDList:
        """Create markdown for tasks with their notes.

        Args:
            tasks (list): Tasks from Todoist

        Returns:
            snakemd.MDList: List with tasks and notes
        """
        task_list = []
        for task in tasks:
            task_list.append(self.md.CheckBox(task['content'], checked=task['checked']))
            # Add task Notes
            notes_list = []
            for note in task['notes']:
                notes_list.append(self.md.Paragraph(note['content']))
            if notes_list:
                task_list.append(self.md.MDList(notes_list))
        return self.md.MDList(task_list)


def generate_markdown():
    """Poetry Command Access to export todoist to markdown."""
    from os.path import join
    app = typer.Typer()

    @app.command()
    def generate_files(output_path: str = "."):
        md = Todoist_Markdown()
        projects = md.generate()
        for f_name, project in projects.items():
            file_path = join(output_path, f_name + ".md")
            rendered = ""
            for element in project._contents:
                padding = "\n\n" if type(element) != snakemd.Header else "\n"
                rendered += str(element) + padding
            with open(file_path, 'w') as outfile:
                outfile.write(rendered)

    app()

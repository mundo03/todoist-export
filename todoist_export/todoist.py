"""Obtain Todoist Full Sync Data.

Copied from
https://github.com/brokensandals/exporteer_todoist/blob/master/src/exporteer_todoist/cli.py
"""

from os import environ
import requests
import typer
import json


class Todoist_Token_Error(Exception):
    """Error Class for when the token is not set."""

    pass


class Todoist():
    """Interact with Todoist."""

    def _get_token(self):
        """Get Todosit Token from Env Variable.

        Raises:
            Todoist_Token_Error: TODOIST_API_TOKEN is Not set

        Returns:
            str: Todoist Api Token
        """
        token = environ.get('TODOIST_API_TOKEN', None)
        if not token:
            raise Todoist_Token_Error('TODOIST_API_TOKEN must be set')
        return token

    def full_sync(self):
        """Get all Data from Todoist.

        Returns:
            dict: all data from Todoist
        """
        token = self._get_token()
        params = {'token': token, 'sync_token': '*', 'resource_types': '["all"]'}
        resp = requests.get('https://api.todoist.com/sync/v8/sync', params=params)
        resp.raise_for_status()
        return resp.json()

    def json(self):
        """Output JSON."""
        raw = self.full_sync()
        return json.dumps(raw, indent=4)


def raw():
    """Poetry Command access to print raw todoist export."""
    app = typer.Typer()

    @app.command()
    def print_raw():
        raw = Todoist().json()
        print(raw)

    app()

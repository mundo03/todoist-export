# Export Todoist to Markdown

Todoist has a pretty flat export, this project inteds to organize the data in a way that makes creating a markdown file easy.

## How to Install

1. Clone
2. `poetry install`

Should work with `pip install .` too, haven't tested it.

This will add two commands for the terminal

## Commands

`todoist_better_export` Prints to STDOUT a hearchical JSON export, looks like this:

``` text
projects:
    - Project Notes
    - Tasks (With out a section)
        - Notes
    - Sections
        - Tasks
            - Notes
```

(Not actual keys, just a representation of the schema)

`todoist_markdown --output-path PATH` Writes the Markdown Files for each projecin `PATH`, defaults to current folder.

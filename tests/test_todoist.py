from todoist_export.todoist import Todoist


def test_sync():
    todoist = Todoist()
    sync_data = todoist.full_sync()
    assert type(sync_data) is dict

from todoist_export.markdown import Todoist_Markdown


def test_todoist_markdown():
    md = Todoist_Markdown()
    try:
        projects = md.generate()
    except Exception as err:
        print(str(err))
        assert False
    for _, project in projects.items():
        assert type(project.render()) is str

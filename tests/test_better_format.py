from todoist_export.better_format import BetterFormat


def test_generate_better_format():
    bf = BetterFormat()
    bf.generate()
    for project in bf.better_data:
        assert project.get('notes', False) is not False
        assert project.get('tasks', False) is not False
        assert project.get('sections', False) is not False
